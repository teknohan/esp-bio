#include <SoftwareSerial.h>

#include <ESP8266WiFi.h>       
#include <Arduino.h>
//needed for library
#include <DNSServer.h>
#include <WebSocketsClient.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>      
#include <Hash.h>
String com;
String comm;
String readString;
SoftwareSerial swSer(14, 12, false, 128);

WebSocketsClient webSocket;
#define USE_SERIAL Serial
void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {

  switch(type) {
    case WStype_DISCONNECTED:
      USE_SERIAL.printf("[WSc] Disconnected!\n");
      break;
    case WStype_CONNECTED: {
      USE_SERIAL.printf("[WSc] Connected to url: %s\n", payload);

      // send message to server when Connected
      webSocket.sendTXT("user_name,password");

      //delay(3000);
     // webSocket.sendTXT("hi server");
      
    }
      break;
    case WStype_TEXT:
      USE_SERIAL.printf("[WSc] get text: %s\n", payload);
       String str = (char*)payload;
        if(str=="test"){
        on_finger_test();
      }
      
      if(str=="search"){
        search();
      }
       if (str == "enroll") {
           Serial.println("please insert page ID");
            while (Serial.available() == 0) { //Wait for user input
          }
          unsigned char ws_comm;
          ws_comm = Serial.read();
            enroll(ws_comm);    
       
    }
        // DO CLIENT JOBS ######################################
      // send message to server
      // webSocket.sendTXT("message here");
      break;
    /*  NEEDED  DELETED COS COULDN'T FIND A WAY
     * case WStype_BIN:
      USE_SERIAL.printf("[WSc] get binary length: %u\n", length);
      hexdump(payload, length);
      // send data to server
      // webSocket.sendBIN(payload, length);
      break;
      */
  }

}







// START ON FINGER TEST#############################################
void on_finger_test()
{
  byte message[] = { 0xef, 0x01, 0xff, 0xff, 0xff, 0xff, 0x01, 0x00, 0x03, 0x01, 0x00, 0x05 }; // check if finger on sensor
    for (int i = 0; i < 100; i++) {
        swSer.write(message, sizeof(message));
        while (swSer.available() == 0) {
        }
        int hex_counter = 0;
        String result = "";
        byte ok = 0x00;
        while (swSer.available() > 0) {
            delay(10); //small delay to allow input buffer to fill

            char c = swSer.read(); //gets one byte from serial buffer
            /* if (c == ','){
           Serial.println("break");
            delay(1000);
            break;
        } //breaks out of capture loop to print readstring*/

            readString += c;

            if (hex_counter == 9) {
                if (c == ok) {
                    result = "ok";
                    //break;
                }

            } //get result
            hex_counter = hex_counter + 1;

        } //makes the string readString

        if (readString.length() > 0) {

            Serial.println(readString); //prints hex string to serial
            delay(10);
            readString = ""; //clears variable for new input
        }
        if (result == "ok") {
            delay(10);
            Serial.println("finger is on the reader");
            break;
        }
        if (i == 99) {
            Serial.println("put your finger on the reader");
        }
    }
}
// END ON FINGER TEST#############################################


// START ON FINGER TEST#############################################
void search(){

  byte step1[] = { 0xef, 0x01, 0xff, 0xff, 0xff, 0xff, 0x01, 0x00, 0x03, 0x01, 0x00, 0x05 }; // check if finger on sensor
  byte step2[] = { 0xef, 0x01, 0xff, 0xff, 0xff, 0xff, 0x01, 0x00, 0x04, 0x02, 0x01, 0x00, 0x08 }; // buffer image
  byte step3[] = { 0xef, 0x01, 0xff, 0xff, 0xff, 0xff, 0x01, 0x00, 0x08, 0x1b, 0x01, 0x00, 0x00, 0x01, 0x2d, 0x00, 0x53 }; // HighSpeedSearch
int found =0;
  for (int i = 0; i < 100; i++) {
        swSer.write(step1, sizeof(step1));
        while (swSer.available() == 0) {
        }
        int hex_counter = 0;
        String result = "";
        byte ok = 0x00;
        while (swSer.available() > 0) {
            delay(10); //small delay to allow input buffer to fill

            char c = swSer.read(); //gets one byte from serial buffer
            readString += c;

            if (hex_counter == 9) {
                if (c == ok) {
                    result = "ok";
                    //break;
                }

            } //get result
            hex_counter = hex_counter + 1;

        } //makes the string readString

        if (readString.length() > 0) {

            Serial.println(readString); //prints hex string to serial
            delay(10);
            readString = ""; //clears variable for new input
        }
        if (result == "ok") {   //    SEDOND STEP ######
            delay(10);
            Serial.println("ready for second step");
            
            swSer.write(step2, sizeof(step2));
            while (swSer.available() == 0) {
            }

            int hex_counter = 0;
            String result = "";
            byte ok = 0x00;
            while (swSer.available() > 0) {
            delay(10); //small delay to allow input buffer to fill

            char c = swSer.read(); //gets one byte from serial buffer

            readString += c;

            if (hex_counter == 9) {
                if (c == ok) {
                    result = "ok";
                    //break;
                }

            } //get result
            hex_counter = hex_counter + 1;

            } //makes the string readString

            if (readString.length() > 0) {

              Serial.println(readString); //prints hex string to serial
              delay(10);
              readString = ""; //clears variable for new input
            }
             if (result == "ok") {  //    LAST STEP ######
               delay(10);
            Serial.println("ready for last step step"); 
            
            swSer.write(step3, sizeof(step3));
            while (swSer.available() == 0) {
            }

            int hex_counter = 0;
            String result = "";
            byte ok = 0x00;
            String user_ID="";
            while (swSer.available() > 0) {
            delay(10); //small delay to allow input buffer to fill

            char c = swSer.read(); //gets one byte from serial buffer

            readString += c;

             if (hex_counter == 10 && found==1 ) {
                
                result+=" id=";
                result += String(c, HEX);
              
                
                
              }
              if(hex_counter == 11 && found==1){
                result += String(c, HEX);
              }
            if (hex_counter == 9) {
                if (c == ok) {
                 found=1;
                    result = "user found";
                    
                }

              

             if (c == 0x09) {
                    result = "USER not found";
                   
                }
                
                

            } //get result
            hex_counter = hex_counter + 1;

            } //makes the string readString

            if (readString.length() > 0) {

              Serial.println(readString); //prints hex string to serial
              delay(10);
              readString = ""; //clears variable for new input
            }
             delay(10);
            Serial.println(result);
            
            
            
            
             }
                            
            break;
        }
        if (i == 99) {
            Serial.println("put your finger on the reader");
        }
    }
  
  
  
}   // END SEARCH#############################################


// START ENROL #############################################
void enroll(unsigned char page_id)
{
byte a1[] = { 0xef, 0x01, 0xff, 0xff, 0xff, 0xff  }; 
byte a2[] = { 0x01, 0x00, 0x03, 0x01, 0x00, 0x05  }; //OK COMMAND
byte a3[] = { 0x01, 0x00, 0x04, 0x02, 0x01, 0x00, 0x08  }; //BUFFER IMAGE1
byte a4[] = { 0x01, 0x00, 0x04, 0x02, 0x02, 0x00, 0x09  }; //BUFFER IMAGE2
byte a5[] = { 0x01, 0x00, 0x03, 0x05, 0x00, 0x09  }; //GENERATE TEMPLATE
unsigned char a6_2[1]{page_id};
byte a6_1[] = { 0xef, 0x01, 0xff, 0xff, 0xff, 0xff, 0x01, 0x00, 0x06, 0x06, 0x02, 0x00 }; //SAVE TEMPLATE
//byte a6_2[] = { 0x06 }; //SAVE TEMPLATE
unsigned char total= a6_1[6] + a6_1[7] + a6_1[8] + a6_1[9] + a6_1[10] + a6_1[11] + a6_2[0]; 
unsigned char a6_3[] ={ 0x00, total  }; 
//byte a6_3[] = { 0x00, 0x15 }; //SAVE TEMPLATE






 /*    step1 */
byte c1[12]="";
memcpy(c1, a1, sizeof(a1));
memcpy(c1+sizeof(a1), a2, sizeof(a2));
Serial.write(c1, HEX);
  

 /*    step2 */
  byte c2[13]="";
memcpy(c2, a1, sizeof(a1));
memcpy(c2+sizeof(a1), a3, sizeof(a3));
Serial.write(c2, HEX);
  
  
 /*    step3 */
  byte c3[12]="";
memcpy(c3, a1, sizeof(a1));
memcpy(c3+sizeof(a1), a2, sizeof(a2));
Serial.write(c3, HEX);
  
  
 /*    step4 */
  byte c4[13]="";
memcpy(c4, a1, sizeof(a1));
memcpy(c4+sizeof(a1), a4, sizeof(a4));
Serial.write(c4, HEX);
  

 /*    step5 */
  byte c5[12]="";
memcpy(c5, a1, sizeof(a1));
memcpy(c5+sizeof(a1), a5, sizeof(a5));
Serial.write(c5, HEX);

 byte c6[15]="";
memcpy(c6, a6_1, sizeof(a6_1));
memcpy(c6+sizeof(a6_1), a6_2, sizeof(a6_2));
memcpy(c6+sizeof(a6_1)+sizeof(a6_2), a6_3, sizeof(a6_3));

Serial.write(c6, HEX);





/* SAMPLE
memcpy(c1, a1, sizeof(a1));
memcpy(c1+sizeof(a1), a2, sizeof(a2));
memcpy(c1+sizeof(a1)+sizeof(a2), a3, sizeof(a3));
*/




/* ENROL STEPS   */
for(int e_step=1; e_step < 7;e_step++ )    { 
delay(10);
Serial.println(e_step);
    for (int i = 0; i < 100; i++) {
      
      if(e_step==1){
        swSer.write(c1, sizeof(c1));
      }
       if(e_step==2){
        swSer.write(c2, sizeof(c2));
      }
       if(e_step==3){
        swSer.write(c3, sizeof(c3));
      }
       if(e_step==4){
        swSer.write(c4, sizeof(c4));
      }
      if(e_step==5){
        swSer.write(c5, sizeof(c5));
      }

      if(e_step==6){
        Serial.write(c6, sizeof(c6));
        swSer.write(c6, sizeof(c6));
      }
      
         while (swSer.available() == 0) {
            }
        int hex_counter = 0;
        String result = "";
        byte ok = 0x00;
        while (swSer.available() > 0) {
            delay(10); //small delay to allow input buffer to fill

            char c = swSer.read(); //gets one byte from serial buffer
            /* if (c == ','){
           Serial.println("break");
            delay(1000);
            break;
        } //breaks out of capture loop to print readstring*/

            readString += c;

            if (hex_counter == 9) {
                if (c == ok) {
                    result = "ok";
                    
                    //break;
                }

            } //get result
            hex_counter = hex_counter + 1;

        } //makes the string readString

        if (readString.length() > 0) {

            Serial.println(readString); //prints hex string to serial
            delay(10);
            readString = ""; //clears variable for new input
        }
        if (result == "ok") {
            delay(10);
             if(e_step==2){
              Serial.println("take your finger off");
              delay (4000);
              Serial.println("put your finger on");
              
             }
            Serial.println("ok");
            delay(100);
            break;
        }
        
        if (i == 99) {
            Serial.println("put your finger on the reader");
           
        }
    }
}
}
// END ENROL#############################################





void hex_test(unsigned char page_id){
byte a6_1[] = { 0xef, 0x01, 0xff, 0xff, 0xff, 0xff, 0x01, 0x00, 0x06, 0x06, 0x02, 0x00 }; //SAVE TEMPLATE
//byte a6_2[] ={0x00, 0x09}; //SAVE TEMPLATE
unsigned char a6_2[1]{page_id};
byte a6_3[] = { 0x00, 0x15 }; //SAVE TEMPLATE

 byte c6[15]="";
memcpy(c6, a6_1, sizeof(a6_1));
memcpy(c6+sizeof(a6_1), a6_2, sizeof(a6_2));
memcpy(c6+sizeof(a6_1)+sizeof(a6_2), a6_3, sizeof(a6_3));
Serial.write(c6, sizeof(c6));
delay(1000);

}




void setup()
{
    Serial.begin(9600);
    swSer.begin(9600);
//Serial.setDebugOutput(true);
  USE_SERIAL.setDebugOutput(true);

  USE_SERIAL.println();
  USE_SERIAL.println();
  USE_SERIAL.println();

  for(uint8_t t = 4; t > 0; t--) {
    USE_SERIAL.printf("[SETUP] BOOT WAIT %d...\n", t);
    USE_SERIAL.flush();
    delay(1000);
    
}
    //WiFiManager
    //Local intialization. Once its business is done, there is no need to keep it around
    WiFiManager wifiManager;
    //reset saved settings
    //wifiManager.resetSettings();
    
    //set custom ip for portal
    //wifiManager.setAPStaticIPConfig(IPAddress(10,0,1,1), IPAddress(10,0,1,1), IPAddress(255,255,255,0));

    //fetches ssid and pass from eeprom and tries to connect
    //if it does not connect it starts an access point with the specified name
    //here  "AutoConnectAP"
    //and goes into a blocking loop awaiting configuration
    wifiManager.autoConnect("AutoConnectAP");
    //or use this for auto generated name ESP + ChipID
    //wifiManager.autoConnect();

    
    //if you get here you have connected to the WiFi
    Serial.println("connected...yeey :)");

    
    // server address, port and URL
 webSocket.begin("128.199.102.10", 9502, "/");

  // event handler
  webSocket.onEvent(webSocketEvent);

  // use HTTP Basic Authorization this is optional remove if not needed
  //webSocket.setAuthorization("user", "Password");

  // try ever 5000 again if connection has failed
webSocket.setReconnectInterval(5000);


    
}

void loop()
{
   webSocket.loop();
int sensorValue = analogRead(4);

 if (sensorValue > 1) {

search();
    delay(1000);
 }
   
   if (Serial.available() > 0) {
    
    com=Serial.readString(); 
    webSocket.sendTXT(com);
      Serial.print(com);
      
      if (com == "test") {
        on_finger_test();
      }
    if (com == "enroll") {
      Serial.println("please insert page ID");
    while (Serial.available() == 0) { //Wait for user input
    }
    unsigned char b_comm;
    b_comm = Serial.read();
        enroll(b_comm);    
       
    }
    if (com == "search") {
        search();
    readString="";
} 


}
}
